: <<'REACTNATIVE'
alias rn="react-native"
alias rnra="react-native run-android"
alias rnri="react-native run-ios"
REACTNATIVE

: <<'JAVASCRIPT'
alias grepj="grepr --js"
alias findj="findm --js"
JAVASCRIPT

#: <<'MISC'
alias prettyjson='python3 -m json.tool'
alias tree="tree -a -I '.git|node_modules|.env|__pycache__'"
